
local me = ...
local plugin = BigWigs:GetPlugin("Bars")
local db = plugin.db.profile
--local colors = BigWigs:GetPlugin("Colors")

local alias = {
	["Energía estática sanguina"] = "CONO" -- Uldir - Taloc
}

-- "bars" l&f
-- We need to setup the bars itself, with a transparent color
local function style(bar)
	bar.candyBarLabel:SetJustifyH("LEFT")
	bar.candyBarLabel:ClearAllPoints()
	bar.candyBarLabel:Point("LEFT", bar, "LEFT", db.BigWigsAnchor_height + 4, 0)

	bar.candyBarDuration:SetJustifyH("LEFT")
	bar.candyBarDuration:ClearAllPoints()
	bar.candyBarDuration:Point("RIGHT", bar, "LEFT", -3, 0)
end

plugin:RegisterBarStyle("GioNoBars", {
	apiVersion = 1,
	version = 1,
	ApplyStyle = style,
	GetStyleName = function() return "Gio No Bars" end,
})

local function styleCreated(msg, plugin, bar, module, key, text, time, icon, isApprox) 
	--DevTools_Dump(colors:GetColor("barText", module, key))
	if alias[text] ~= nil then
		bar:SetLabel(alias[text] .." (".. text ..")")
	end
end

local function styleEmphasized(msg, plugin, bar) 
	bar:SetTextColor(1, 0, 0) -- this should be fixed
	--local r, g, b, a = bar.candyBarLabel:GetTextColor()
	--print(r)
	--print(g)
	--print(b)
	--print(a)
end

BigWigsLoader.RegisterMessage(me, "BigWigs_BarCreated", styleCreated)
BigWigsLoader.RegisterMessage(me, "BigWigs_BarEmphasized", styleEmphasized)
